﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Diffuse - Worldspace" {
	Properties {
		_ColorX ("Color X", Color) = (1,.5,0,1)
		_ColorY ("Color Y", Color) = (.1,.1,.1,1)
		_ColorZ ("Color Z", Color) = (1,.5,0,1)
		_TexX ("X Axis", 2D) = "white" {}
		_TexY ("Y Axis", 2D) = "white" {}
		_TexZ ("Z Axis", 2D) = "white" {}
		_Scale ("Texture Scale", Vector) = (1,1,1,1)
	}
	SubShader {
		Tags 
		{ 
			"RenderType" = "Opaque" 
			"DisableBatching" = "True"
		}
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert

		sampler2D _TexX;
		sampler2D _TexY;
		sampler2D _TexZ;
		float4 _TexX_ST;
		float4 _TexY_ST;
		float4 _TexZ_ST;
		fixed4 _ColorX;
		fixed4 _ColorY;
		fixed4 _ColorZ;
		fixed4 _Scale;
		float4x4 _ScaleMatrix;

		struct Input {
			float3 localNormal;
			float3 localPos;
		};

		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input,o);
		    float3 recipObjScale = float3( length(unity_WorldToObject[0].xyz), length(unity_WorldToObject[1].xyz), length(unity_WorldToObject[2].xyz) );
		    float3 objScale = 1.0/recipObjScale;
		    float4x4 mat = float4x4( 1/objScale.x, 0.0, 0.0, 0.0,
		    				 		 0.0, 1/objScale.y, 0.0, 0.0,
		    				 		 0.0, 0.0, 1/objScale.z, 0.0,
		    				 		 0.0, 0.0, 0.0, 1.0 );
			o.localPos = objScale * v.vertex.xyz;
			//o.localNormal = v.normal;
			o.localNormal = normalize(mul(mat, v.normal));
		}

		void surf (Input IN, inout SurfaceOutput o) {
			float2 mult;
			float2 UV;
			fixed4 c;
			
			if(abs(IN.localNormal.x) > 0.72) {
				mult = float2(sign(IN.localNormal.x),1);
				UV = (mult * IN.localPos.zy) + _TexX_ST.ba;
				c = tex2D(_TexX, UV* _Scale.zy) * _ColorX;
			} 
			else if(abs(IN.localNormal.z) > 0.72) {
				mult = float2(-sign(IN.localNormal.z),1);
				UV = (mult * IN.localPos.xy) + _TexZ_ST.ba;
				c = tex2D(_TexZ, UV* _Scale.xy) * _ColorZ;
			}
			else if (abs(IN.localNormal.y) > 0.72) {
				mult = float2(sign(IN.localNormal.y), 1);
				UV = (mult * IN.localPos.xz) + _TexY_ST.ba;
				c = tex2D(_TexY, UV* _Scale.xz) * _ColorY;
			}
			else {
				mult = float2(sign(IN.localNormal.y),1);
				UV = (mult * IN.localPos.xz) + _TexY_ST.ba;
				c = tex2D(_TexY, UV* _Scale.xz) * _ColorY;
			}
			//0.70711

			o.Albedo = c.rgb;// *_Color;
		}
		ENDCG
	}

	Fallback "VertexLit"
}
