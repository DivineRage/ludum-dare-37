﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonLimb : MonoBehaviour {

    public Person person;



    private void Awake() {
        person = GetComponentInParent<Person>();
    }

    private void OnCollisionEnter(Collision collision) {
        Debug.LogFormat("OnCollisionEnter", collision.gameObject);
        person.Kill();
    }

}
