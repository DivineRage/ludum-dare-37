﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointOfInterest : MonoBehaviour {

    public static List<PointOfInterest> Instances = new List<PointOfInterest>();

    private void Awake() {
        Instances.Add(this);
    }

    private void OnDestroy() {
        Instances.Remove(this);
    }

}
