﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Isle : MonoBehaviour {

    public int isleIndex = 0;
    public Transform indicator;
    public Transform line;
    public Text text;
    public Color colorContained = Color.green;
    public Color colorIrrelevant = Color.gray;
    public Rack[] racksPositive;
    public Rack[] racksNegative;



    private void Awake() {
        if (text == null)
            text = GetComponentInChildren<Text>();
        Player.OnObjectiveChanged += OnUpdateObjective;
    }

    private void OnUpdateObjective(Objective o) {
        var side = o.objectiveTarget.spawnPoint.shelfLayout.shelf.side;
        var rack = o.objectiveTarget.spawnPoint.shelfLayout.shelf.rack;
        var contained = (side ? racksNegative : racksPositive).Contains(rack);

        text.color = contained ? colorContained : colorIrrelevant;
        indicator.gameObject.SetActive(contained);
        line.gameObject.SetActive(contained);
        if (contained) {
            var position = transform.InverseTransformPoint(o.objectiveTarget.transform.position);
            position.x = 0f;
            position.y = 0f;
            var lineLength = position.z;
            position = transform.TransformPoint(position);
            indicator.position = position;
            indicator.localEulerAngles = new Vector3(0f, side ? 0f : 180f, 0f);
            lineLength -= 2f;
            lineLength -= .6f;
            line.localScale = new Vector3(.5f, 1f, lineLength);
        }
    }

    private void OnValidate() {
        text = GetComponentInChildren<Text>();
        if (text != null)
            text.text = isleIndex.ToString();

        gameObject.name = string.Format("Isle {0}", isleIndex);
    }

}
