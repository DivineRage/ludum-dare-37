﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackageSpawn : MonoBehaviour {

    public BoxCollider boxCollider;
    public ShelfLayout shelfLayout;
    public Package package;

    private void Awake() {
        boxCollider = GetComponent<BoxCollider>();
    }

    private void OnDestroy() {
        shelfLayout = null;
    }



    public float GetMaxWidth() {
        return boxCollider.size.x;
    }

}
