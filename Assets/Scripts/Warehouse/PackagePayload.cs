﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackagePayload : MonoBehaviour {

    public string packageName = "Payload";
    public string packageDescription = "VERY DELICATE!";
    public float width = 1.6f;
    public float height = .8f;
    public Rigidbody[] childRigidbodies;

    private void Awake() {
        childRigidbodies = GetComponentsInChildren<Rigidbody>(true);
        foreach(var rb in childRigidbodies) {
            rb.Sleep();
        }
    }

}
