﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarehouseManager : MonoBehaviour {

    public static float KillFloor = -5f;

    public System.Action OnDonePopulating = () => { };
    public Rack[] racks;

	IEnumerator Start () { // I can never figure out which of them is the right one...
        racks = GetComponentsInChildren<Rack>();
        Debug.LogFormat("Populating {0} racks.", racks.Length);
        for (int i = 0; i < racks.Length; i++) {
            racks[i].Populate();
            yield return null;
        }

        OnDonePopulating();
	}
	


    public Package GetRandomPackage() {
        int tries = 3;
        do {
            var rack = racks.ElementAt(Random.Range(0, racks.Length));
            var package = rack.GetRandomPackage();
            if (package != null)
                return package;
        } while (tries-- > 0);
        return FindObjectsOfType<Package>().First();
    }

}
