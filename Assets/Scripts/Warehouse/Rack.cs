﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rack : MonoBehaviour {

    public Package packagePrefab;
    public PointOfInterest pointOfInterestPrefab;
    public Shelf[] shelves;
    [Range(0f,1f)]
    public float populationChance = .6f;


    private void Awake() {
        shelves = GetComponentsInChildren<Shelf>();
        foreach(var shelf in shelves) {
            shelf.rack = this;
        }
    }

    public void Populate() {
        foreach(var shelf in shelves) {
            foreach(var spawn in shelf.layout.spawns) {
                if (!(Random.Range(0f, 1f) <= populationChance))
                    continue;

                var payloadPrefab = GetPayloadForSize(spawn.GetMaxWidth(), shelf.maxHeight - Package.PalletHeight);
                if (payloadPrefab == null)
                    continue;

                var package = Instantiate(packagePrefab, spawn.transform, false);
                package.transform.rotation = GetPackageRotation(spawn);

                var payload = Instantiate(payloadPrefab);
                payload.transform.position = package.payloadParent.position;
                payload.transform.rotation = package.payloadParent.rotation;

                package.Assign(spawn);
                package.payload = payload;
            }

            if (shelf.transform.localPosition.y == 0f) {
                CreatePointOfInterestForShelf(shelf);
            }
        }
    }

    private void CreatePointOfInterestForShelf(Shelf shelf) {
        foreach(var spawn in shelf.layout.spawns) {
            var localpos = shelf.transform.InverseTransformPoint(spawn.transform.position);
            localpos.y = 0f;
            localpos.z = 0f;
            localpos += new Vector3(0f, 0f, -.5f);
            var poi = Instantiate(pointOfInterestPrefab, transform);
            poi.transform.position = shelf.transform.TransformPoint(localpos);
        }
    }

    public Quaternion GetPackageRotation(PackageSpawn spawn) {
        return spawn.transform.rotation;
    }

    public Package GetRandomPackage() {
        var packages = GetComponentsInChildren<PackageSpawn>().Where(s => s.package != null);
        if (packages.Count() == 0)
            return null;
        return packages.ElementAt(Random.Range(0, packages.Count())).package;
    }



    private static PackagePayload[] _payloads;
    public static PackagePayload[] Payloads {
        get {
            if (_payloads == null) {
                _payloads = Resources.LoadAll<PackagePayload>("Package Payloads");
                Debug.LogFormat("Found {0} payloads.", _payloads.Length);
            }
            return _payloads;
        }
    }

    public static PackagePayload GetPayloadForSize(float width, float height) {
        var matching = Payloads.Where(p => p.width <= width && p.height <= height);
        if (matching.Count() == 0)
            return null;
        return matching.ElementAt(Random.Range(0, matching.Count()));
    }

}
