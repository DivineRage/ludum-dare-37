﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShelfLayout : MonoBehaviour {

    public Shelf shelf;
    public PackageSpawn[] spawns;

    private void Awake() {
        spawns = GetComponentsInChildren<PackageSpawn>();
        foreach (var spawn in spawns) {
            spawn.shelfLayout = this;
        }
    }

    public void UpdateSpawnsForShelf(Shelf shelf) {
        foreach (var spawn in spawns) {
            var size = spawn.boxCollider.size;
            spawn.boxCollider.size = new Vector3(size.x, shelf.maxHeight, size.z);
            spawn.boxCollider.center = new Vector3(0f, shelf.maxHeight / 2f, 0f);
        }
    }

}
