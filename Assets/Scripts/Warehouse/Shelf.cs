﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Shelf : MonoBehaviour {

    public float maxHeight;
    public bool side;

    public Rack rack;
    public ShelfLayout layout;

    private void Awake() {
        layout = Instantiate(ShelfLayouts.ElementAt(Random.Range(0, ShelfLayouts.Length)), transform);
        layout.transform.localPosition = Vector3.zero;
        layout.transform.localRotation = Quaternion.identity;
        layout.shelf = this;
    }

    private void OnValidate() {
        side = (transform.localPosition.x < 0);
    }


    private static ShelfLayout[] _shelfLayouts;
    public static ShelfLayout[] ShelfLayouts {
        get {
            if (_shelfLayouts == null) {
                _shelfLayouts = Resources.LoadAll<ShelfLayout>("Shelf Layouts");
                Debug.LogFormat("Found {0} shelf layouts.", _shelfLayouts.Length);
            }
            return _shelfLayouts;
        }
    }

}
