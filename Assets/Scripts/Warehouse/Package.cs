﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class Package : MonoBehaviour {

    public static float PalletHeight = .4f;

    public Transform payloadParent;
    public Rigidbody rb;

    public PackagePayload payload;
    public PackageSpawn spawnPoint;
    public event System.Action<Package> OnDestroyed = (p) => { };
    private bool isQuitting = false;


    private void Awake() {
        rb = GetComponent<Rigidbody>();
        rb.Sleep();
    }

    private void Update() {
        if (transform.position.y <= WarehouseManager.KillFloor) {
            if (payload)
                Destroy(payload.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnApplicationQuit() {
        isQuitting = true;
    }

    private void OnDestroy() {
        if (!isQuitting)
            OnDestroyed(this);
    }


    public void Assign(PackageSpawn spawn) {
        spawnPoint = spawn;
        spawn.package = this;
    }

    public void MakeObjective(Objective obj) {
        var renderers = GetComponentsInChildren<Renderer>();
        foreach (var r in renderers) {
            r.material.color = Color.green;
        }
        OnDestroyed += (p) => { obj.Complete(); };
    }

}
