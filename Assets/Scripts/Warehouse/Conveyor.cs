﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class Conveyor : MonoBehaviour {

    public Vector3 localDirection = Vector3.right;
    public float speed = 5f;
    public float force = .5f;
    public ForceMode forceMode = ForceMode.VelocityChange;

    void OnCollisionStay(Collision collisionInfo) {
        if (collisionInfo.gameObject.GetComponentInParent<ForkliftController>() != null)
            return;

        var rb = collisionInfo.rigidbody;
        if (rb == null)
            return;

        rb.WakeUp();

        var conveyorDirection = transform.TransformDirection(localDirection);
        var currentVelocityInDirection = rb.velocity;
        currentVelocityInDirection.Scale(conveyorDirection);

        if (currentVelocityInDirection.magnitude < speed) {
            rb.AddForce(conveyorDirection * force, forceMode);
        }
    }

}
