﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Person : MonoBehaviour {

    public Animator animator;
    public NavMeshAgent navAgent;
    private bool dead;

    public float distanceThreshold = .3f;
    public float idleTime = 2f;


    private void Start() {
        StartCoroutine(BehaviourCoroutine());
    }

    private void Update() {
        if (animator && navAgent)
            animator.SetBool("Walking", navAgent.remainingDistance > distanceThreshold);
    }

    private IEnumerator BehaviourCoroutine() {
        while (!dead) {
            navAgent.SetDestination(PointOfInterest.Instances.ElementAt(Random.Range(0, PointOfInterest.Instances.Count)).transform.position);
            navAgent.Resume();
            yield return new WaitUntil(() => { return navAgent.remainingDistance < distanceThreshold; });
            yield return new WaitForSeconds(idleTime);
        }
    }



    [ContextMenu("Kill")]
    public void Kill() {
        if (dead)
            return;

        dead = true;

        animator.Stop();
        animator.SetTime(0.0);
        navAgent.Stop();

        Destroy(animator);
        Destroy(navAgent);

        var skinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach(var smr in skinnedMeshRenderers) {
            for (int i = 0; i < smr.sharedMesh.blendShapeCount; i++) {
                smr.SetBlendShapeWeight(i, 0f);
            }
        }

        var colliders = GetComponentsInChildren<Collider>(true);
        foreach(var col in colliders) {
            col.enabled = true;
            var rb = col.gameObject.AddComponent<Rigidbody>();
        }
    }

}
