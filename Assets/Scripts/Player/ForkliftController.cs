﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class ForkliftController : MonoBehaviour {

    public Transform liftBase;
    public ForkliftArmData[] liftingParts;
    public WheelCollider[] frontWheels;
    public WheelCollider[] backWheels;
    public Rigidbody rb;
    public Transform centerOfMass;

    public float acceleration = 10f;
    public float turningSpeed = 10f;
    public float maxSpeed = 10f;
    public float brakeTorqueFront = 100f;
    public float brakeTorqueBack = 100f;
    public float turningAngle = 30f;

    public float liftSpeed = .1f;
    public float liftRotateSpeed = .1f;
    public float liftAngleMin = -5f;
    public float liftAngleMax = 5f;


    float inputSteering = 0f;
    float inputThrottle = 0f;
    float inputLift = 0f;
    float inputLiftTilt = 0f;



	void Start () {
        rb.centerOfMass = centerOfMass.localPosition;
        for (int iWheel = 0; iWheel < frontWheels.Length; iWheel++) {
            frontWheels[iWheel].brakeTorque = brakeTorqueFront;
        }
        for (int iWheel = 0; iWheel < backWheels.Length; iWheel++) {
            backWheels[iWheel].brakeTorque = brakeTorqueBack;
        }
    }
	
	void Update () {
        #region Steering
        var hor = Input.GetAxisRaw("Horizontal");

        if (hor != 0f) {
            inputSteering = Mathf.Clamp(inputSteering - (hor * turningSpeed * Time.deltaTime), -1f, 1f);
        }
        else if (inputSteering != 0f) {
            if (inputSteering > 0) {
                inputSteering = Mathf.Clamp01(inputSteering - (turningSpeed * Time.deltaTime));
            } else {
                inputSteering = Mathf.Clamp(inputSteering + (turningSpeed * Time.deltaTime), -1f, 0f);
            }
        }
        for (int iWheel = 0; iWheel < backWheels.Length; iWheel++) {
            backWheels[iWheel].steerAngle = inputSteering * turningAngle;
        }
        #endregion

        #region Throttle
        var ver = Input.GetAxisRaw("Vertical");

        if (ver != 0f) {
            inputThrottle = Mathf.Clamp(inputThrottle + (ver * acceleration * Time.deltaTime), -1f, 1f);
        }
        else if (inputThrottle != 0f) {
            if (inputThrottle > 0) {
                inputThrottle = Mathf.Clamp01(inputThrottle - (acceleration * Time.deltaTime));
            }
            else {
                inputThrottle = Mathf.Clamp(inputThrottle + (acceleration * Time.deltaTime), -1f, 0f);
            }
        }
        for (int iWheel = 0; iWheel < backWheels.Length; iWheel++) {
            frontWheels[iWheel].motorTorque = inputThrottle * maxSpeed;
            backWheels[iWheel].motorTorque = inputThrottle * maxSpeed;
            frontWheels[iWheel].brakeTorque = ver == 0f ? brakeTorqueFront : 0f;
            backWheels[iWheel].brakeTorque = ver == 0f ? brakeTorqueBack : 0f;
        }
        #endregion

        #region Lift
        var lift = Input.GetAxis("Lift Movement");
        inputLift = Mathf.Clamp01(inputLift + (lift * liftSpeed * Time.deltaTime));
        foreach(var liftingPart in liftingParts) {
            // Speed depends on maxLiftHeight
            // Damnit I should've paid attention to lift arm transforms...
            liftingPart.transform.localPosition = liftingPart.restPosition + (Vector3.forward * liftingPart.maxLiftHeight * inputLift);
        }
        #endregion

        #region Lift Tilt
        var liftTilt = Input.GetAxis("Lift Tilt");
        inputLiftTilt = Mathf.Clamp(inputLiftTilt + (liftTilt * liftRotateSpeed), -1f, 1f);
        liftBase.localEulerAngles = new Vector3(-90 + (inputLiftTilt * (Mathf.Sign(inputLiftTilt) > 0 ? liftAngleMax : -liftAngleMin)), 0f, 0f);
        #endregion

    }

    void FixedUpdate() {

    }









}
