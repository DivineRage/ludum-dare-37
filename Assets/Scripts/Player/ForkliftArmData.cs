﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkliftArmData : MonoBehaviour {

    public float maxLiftHeight = 1.5f;

    public Vector3 restPosition;

    void Start() {
        restPosition = transform.localPosition;
    }

}
