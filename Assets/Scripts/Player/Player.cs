﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static event System.Action<Objective> OnObjectiveChanged = (o) => { };

    public WarehouseManager warehouse;

    private Objective _currentObjective;
    public Objective CurrentObjective {
        get {
            return _currentObjective;
        }
        set {
            _currentObjective = value;
            OnObjectiveChanged(_currentObjective);
        }
    }



	void Start () {
        warehouse = FindObjectOfType<WarehouseManager>();
        warehouse.OnDonePopulating += () => { CreateNewObjective(); };
	}

    void Update() {

    }



    public void CreateNewObjective() {
        var objectiveTarget = warehouse.GetRandomPackage();
        if (objectiveTarget == null) {
            Debug.LogWarning("Could not find a package to make into an objective.");
            return;
        } else {
            CurrentObjective = new Objective(this, objectiveTarget);
        }
    }

}
