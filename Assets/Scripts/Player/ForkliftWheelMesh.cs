﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForkliftWheelMesh : MonoBehaviour {

    public WheelCollider wheel;
	
	void LateUpdate() {
        transform.localEulerAngles = new Vector3(0f, wheel.steerAngle, 0f);
	}

}
