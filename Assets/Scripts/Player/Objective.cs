﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objective {

    public static event System.Action<Objective> OnObjectiveCompleted = (o) => { };

    public Player objectiveOwner;
    public Package objectiveTarget;

    public Objective(Player player, Package objective) {
        objectiveOwner = player;
        objectiveTarget = objective;
        objectiveTarget.MakeObjective(this);
    }

    public void Complete() {
        Debug.Log("Objective Completed!");
        OnObjectiveCompleted(this);
        objectiveOwner.CreateNewObjective();
    }

}
