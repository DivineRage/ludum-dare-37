﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float speed = 150f;

	void Update () {
        transform.RotateAround(transform.position, Vector3.up, Input.GetAxis("Mouse X") * speed * Time.deltaTime);
        transform.RotateAround(transform.position, transform.right, -Input.GetAxis("Mouse Y") * speed * Time.deltaTime);
    }

}
