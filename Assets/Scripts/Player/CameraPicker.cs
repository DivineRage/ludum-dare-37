﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPicker : MonoBehaviour {

    public List<GameObject> cameras;
    public int activeIndex = 0;

	void Update () {
		if (Input.GetKeyDown(KeyCode.C)) {
            activeIndex = (activeIndex + 1) % cameras.Count;
            for (int i = 0; i < cameras.Count; i++) {
                cameras[i].SetActive(i == activeIndex);
            }
                
        }
	}

}
