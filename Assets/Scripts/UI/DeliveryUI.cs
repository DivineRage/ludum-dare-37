﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DeliveryUI : MonoBehaviour {

    public GameObject DeliveryCompletedPopup;
    public float deliveryPopupDuration = 2f;

    private Text _text;
    public Text TextComponent {
        get {
            if (_text == null)
                _text = GetComponent<Text>();
            return _text;
        }
    }
    public string formatting = "Delivery Requested:\n- {0}x {1}";

    private void Start() {
        Player.OnObjectiveChanged += OnObjectiveChanged;
        Objective.OnObjectiveCompleted += (o) => { StartCoroutine(OnObjectiveComplete(o)); };
    }

    private IEnumerator OnObjectiveComplete(Objective obj) {
        if (DeliveryCompletedPopup) {
            DeliveryCompletedPopup.SetActive(true);
            yield return new WaitForSeconds(deliveryPopupDuration);
            DeliveryCompletedPopup.SetActive(false);
        }
    }

    private void OnObjectiveChanged(Objective obj) {
        TextComponent.text = string.Format(formatting, 1, obj.objectiveTarget.payload.packageName);
    }

}
